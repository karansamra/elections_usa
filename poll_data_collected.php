<?php
include('header.php');
 $sql = "SELECT TD.*, ST.id as st_id, TH.first_name, TH.last_name, TH.email
FROM ticket_data  TD

LEFT JOIN service_tickets ST
ON TD.service_ticket_id=ST.id

LEFT JOIN technician TH
ON ST.technician_id=TH.id

ORDER BY TD.created_at"; //die;

$result=mysqli_query($db,$sql);


?>
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Collected Polling Site Data</a></li>
			</ul>
			<a href="assign_polling_venues.php">Data Collected</a>
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Collected Polling Site Data</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable">
						  <thead>
							  <tr>
								  <th>Technician Name</th>
								  <th>Email</th>
								  <th>Clerk Name</th>
								  <th>Insp. drop-off Loc</th>
								  <th>Votes</th>
								  <th>Cellphone</th>
								  <th>Comments</th>
								  <th>Submitted On</th>
								  
							  </tr>
						  </thead>   
						  <tbody>
						  <?php
  while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
   $object = $results_users;
	$id = $object['id'];
	$clerk_name = $object['clerk_name'];
	$inspector_dropoff_loc = $object['inspector_dropoff_loc'];
	$num_votes_cast = $object['num_votes_cast'];
	$comments = $object['comments'];
	$cellphone = $object['cellphone'];
	$submitted_on = $object['created_at'];
	
  ?>
							<tr id="tr_<?php echo $id; ?>">
								<td><?php echo $object['first_name'].' '.$object['last_name'];?></td>
								 <td><?php echo $object['email']; ?></td>
								<td><?php echo $clerk_name; ?></td>
								<td><?php echo $inspector_dropoff_loc; ?></td>
								<td class="center">
									<?php echo $num_votes_cast; ?>
								</td>
								<td><?php echo $cellphone; ?></td>
								<td><?php echo $comments; ?></td>
								<td><?php echo $submitted_on; ?></td>
								
							</tr>
							     <?php
}?>                                
							  </tbody>
						 </table>  
						  
					</div>
				</div><!--/span-->
			</div><!--/row-->
    

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">�</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
<script type="text/javascript">
$(document).ready(function(){


	$('.bootstrap-datatable').dataTable({
        "aLengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
        "iDisplayLength": 20
    });
			
})
</script>
	<style>
div.dataTables_filter {
    float: right;
    width: 275px;
}
div.dataTables_length {
    display: inline-block;
    width: 310px;
}
div.dataTables_info {
    display: inline-block;
    float: left;
    width: 450px;
}
div.dataTables_paginate {
    display: inline-block;
    float: right;
}
.dataTables_paginate a {
    background: #f0f0f0 none repeat scroll 0 0;
    border: 1px solid grey;
    cursor: pointer;
    display: inline-block;
    margin-left: 3px;
    padding: 5px;
    text-decoration: none;
}
</style>
	
<?php
include('footer.php');
?>