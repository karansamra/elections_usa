<?php
include('config.php');
function get_values_technicians( $db ){
$sql="SELECT * FROM technician";
$result=mysqli_query($db,$sql);

$return_array = array();
$i=0;
while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 

	$object = $results_users;
	$return_array[$i]['id'] = $object['id'];
	$return_array[$i]['first_name']= $object['first_name'];
	$return_array[$i]['last_name'] = $object['last_name'];
	$return_array[$i]['email'] = $object['email'];
	$return_array[$i]['lat'] = $object['latitude'];
	$return_array[$i]['long'] = $object['longitude'];
	$i++;
	}
//print_r($return_array);
	return $return_array;
}
 // -======================= GET ALL POLL VENUES ==========================
function get_values_poll_venues($db){
$sql="SELECT * FROM poll_venues";
$result=mysqli_query($db,$sql);

$return_array = array();
$i=0;
while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 

	$object = $results_users;
	$return_array[$i]['id'] = $object['id'];
	$return_array[$i]['municipality']= $object['municipality'];
	$return_array[$i]['voting_district']= $object['voting_district'];
	$return_array[$i]['latitude']= $object['latitude'];
	$return_array[$i]['longitude']= $object['longitude'];
	$return_array[$i]['ST']= $object['ST'];
	$return_array[$i]['location_poll']= $object['name_of_location'];
	$return_array[$i]['ZIP']= $object['ZIP'];
	$return_array[$i]['address']= $object['address_line_1'].', '.$object['ST'].', '.$object['ZIP'];
	$return_array[$i]['name_of_location']= $object['name_of_location'].', '.$object['ST'];
	$return_array[$i]['location_nm']= $object['name_of_location'];
	$return_array[$i]['ward'] = $object['ward'];
	$return_array[$i]['is_assigned'] = $object['is_assigned'];
	$return_array[$i]['assigned_to'] = $object['assigned_to'];
	$i++;
	}
//print_r($return_array);
	return $return_array;
}

/**
 * Returns information about service tickets 
 * in the form of array with respect to technician id
 * passed.
 * 
 * @param integer $technician_id
 * @author Jaskaran Singh
 * @version 1.0
 * @return array
 */
function getServiceTicketsByTechnician( $db, $technician_id )
{
//	$sql="SELECT * FROM service_tickets where `technician_id` = ".$technician_id;
	$sql = "SELECT ST.*, PV.ward, PV.address_line_1, PV.address_line_2, PV.ST, PV.ZIP , PV.name_of_location,PV.voting_district, PV.latitude, PV.longitude, TH.first_name, TH.last_name, TH.email
			FROM service_tickets  ST

			LEFT JOIN poll_venues PV
			ON ST.polling_site_id=PV.id

			LEFT JOIN technician TH
			ON ST.technician_id=TH.id
			WHERE ST.technician_id ='$technician_id'
			ORDER BY ST.id";
	$result=mysqli_query($db,$sql);
	
	$return_array = array();
	$i=0;
	while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
	
		$object = $results_users;
		if($object['status'] == '0'){
		$status = 'Open';
		}
		else{
		$status = 'Closed';
		
		}
		$return_array[$i]['id'] = $object['id'];
		$return_array[$i]['polling_site_id'] = $object['polling_site_id'];
		$return_array[$i]['voting_district']= $object['voting_district'];
		$return_array[$i]['assigned_on'] = $object['created_at'];
		$return_array[$i]['poll_ward'] = $object['ward'];
		$return_array[$i]['priority_ticket'] = $object['priority_ticket'];
		$return_array[$i]['machine_num'] = $object['machine_num'];
		$return_array[$i]['poll_site_name'] = $object['name_of_location'];
		$return_array[$i]['poll_site_address'] = $object['address_line_1'].', '.$object['ST'].', '.$object['ZIP'];
		$return_array[$i]['lat'] = $object['latitude'];
		$return_array[$i]['long'] = $object['longitude'];
		$return_array[$i]['status'] = $status;
		$return_array[$i]['poll_site_address'] = $object['address_line_1'].', '.$object['ST'].', '.$object['ZIP'];
		
		
		$return_array[$i]['technician_id']= $object['technician_id'];
		//$return_array[$i]['address']= $object['address'];
		$return_array[$i]['reason_call']= $object['reason_call'];
		$return_array[$i]['supply_needed']= $object['supply_needed'];
		$i++;
		}
	// print_r($return_array);
	return $return_array;
}

// ------------------- GET COMMON SUPPLIES --------------

function get_common_supplies($table_name, $field_name,$db){
$sql="SELECT * FROM $table_name";
$result=mysqli_query($db,$sql);

$return_array = array();
$i=0;
while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 

	$object = $results_users;
	$return_array[$i]['id'] = $object['id'];
	$return_array[$i]['common_supply']= $object['common_supply'];
	
	$i++;
	}
//print_r($return_array);
	return $return_array;
}
// ------------------- GET CALL REASONS --------------

function get_call_reasons($table_name, $field_name,$db){
$sql="SELECT * FROM $table_name";
$result=mysqli_query($db,$sql);

$return_array = array();
$i=0;
while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 

	$object = $results_users;
	$return_array[$i]['id'] = $object['id'];
	$return_array[$i]['call_reason']= $object['call_reason'];
	
	$i++;
	}
//print_r($return_array);
	return $return_array;
}

// ================== GET SERVICE TICKET DATA FOR PRINT IN PDF ==================

function getServiceTicketData($id){
global $db;
$sql = "SELECT ST.*, PV.ward, PV.address_line_1, PV.address_line_2, PV.voting_district, PV.ST, PV.ZIP , PV.name_of_location, PV.latitude, PV.longitude, TH.first_name, TH.last_name, TH.email
			FROM service_tickets  ST

			LEFT JOIN poll_venues PV
			ON ST.polling_site_id=PV.id

			LEFT JOIN technician TH
			ON ST.technician_id=TH.id
			WHERE ST.id ='$id'
			ORDER BY ST.id";
	$result=mysqli_query($db,$sql);
	
	$return_array = array();
	$i=0;
	while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
	
		$object = $results_users;
		if($object['status'] == '0'){
		$status = 'Open';
		}
		else{
		$status = 'Closed';
		
		}
		$object['id'] = sprintf('%06d', $object['id']);
		$return_array[$i]['id'] = $object['id'];
		$return_array[$i]['polling_site_id'] = $object['polling_site_id'];
		$return_array[$i]['voting_district']= $object['voting_district'];
		$return_array[$i]['assigned_on'] = $object['created_at'];
		$return_array[$i]['poll_ward'] = $object['ward'];
		$return_array[$i]['priority_ticket'] = $object['priority_ticket'];
		$return_array[$i]['machine_num'] = $object['machine_num'];
		$return_array[$i]['poll_site_name'] = $object['name_of_location'];
		$return_array[$i]['poll_site_address'] = $object['address_line_1'].', '.$object['ST'].', '.$object['ZIP'];
		$return_array[$i]['lat'] = $object['latitude'];
		$return_array[$i]['long'] = $object['longitude'];
		$return_array[$i]['status'] = $status;
		$return_array[$i]['poll_site_address'] = $object['address_line_1'].', '.$object['ST'].', '.$object['ZIP'];
		
		
		$return_array[$i]['technician_id']= $object['technician_id'];
		$return_array[$i]['first_name']= $object['first_name'];
		$return_array[$i]['last_name']= $object['last_name'];
		$return_array[$i]['email']= $object['email'];
		//$return_array[$i]['address']= $object['address'];
		$return_array[$i]['reason_call']= $object['reason_call'];
		$return_array[$i]['supply_needed']= $object['supply_needed'];
		
		$i++;
		}
	// print_r($return_array);
	return $return_array;

}

function sendPushNotificationToTechnician($technician_id, $message, $ticket_id) { 
    global $db;
    $sql = "Select push_regid from technician where id = '$technician_id' ";
    $result = mysqli_query($db, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $count = mysqli_num_rows($result);
    if ($count > 0) {
        $msg = array
            (
            'message' => $message,
            'service_ticket_id' => $ticket_id,
            'technician_id'   => $technician_id,
            'service_ticket_data' => getServiceTicketData($ticket_id) 
        );
        //Old
        $registrationIds = array($row['push_regid']);
        //Latest
        /* $registrationIds = array("APA91bFmaluU3botkCK8qsu8chbXzDZFpkiIif3cbY9D5CbViiFyvoqfAfH9jaknrxcXxZewFqcMPhWJpMkcxYmdtyDKVnClfGqAgTQsqtWCOmrm-zcWcqXVn20gCuJ98GJ1oihGbzFe"); */
        $fields = array
            (
            'registration_ids' => $registrationIds,
            'data' => $msg
        );

        $headers = array
            (
            'Authorization: key=' . 'AIzaSyB2GkpTvUm3VqlmfQqXcphTYWuxqGQK46U',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        //print_r($result);
    }
}
?>