<?php
include ('header.php');

//echo '<pre>';
//print_r( $_POST );
//die;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;





if( $_POST )
{
	
	$querylog = new ParseQuery ( "logBookNewEntry" );
	//$querylog->includeKey("learnersInfo");
	//$querylog->equalTo("key","val")
	//$querylog->equalTo("key","val")
	//$querylog->equalTo("key","val")
	$results = $querylog->find();
	//print_r($results); die;
	?>
	<!-- start: Content -->
	<div id="content" class="span10">
	
	
	<ul class="breadcrumb">
		<li><i class="icon-home"></i> <a href="users.php">Home</a> <i
			class="icon-angle-right"></i></li>
		<li><a href="#">Audit Report</a></li>
	</ul>
	
	<div class="row-fluid sortable">
	<div class="box span12">
	<div class="box-header" data-original-title>
	<h2><i class="halflings-icon user"></i><span class="break"></span>Audit Report</h2>
	<div class="box-icon"><a href="#" class="btn-minimize"><i
		class="halflings-icon chevron-up"></i></a> <a href="#"
		class="btn-close"><i class="halflings-icon remove"></i></a></div>
	</div>
	<div class="box-content">
	<script type="text/javascript">
	$('#datatable').dataTable( {
		  "columns": [
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "5%" },
		    { "width": "15%" },
		  ]
		} );
	
	</script>
	
	
	<table
		class="table table-striped table-bordered bootstrap-datatable datatable">
		<thead>
			<tr>
				<th>Learner</th>
				<th>Learner's Licence Number</th>
				<th>Supervisor</th>
				<th>Supervisor's Licence Number</th>
				<th>Driving Time</th>
				<th>Driving Mode</th>
				<th>Duration Trip</th>
				<th>Vehicle</th>
				<th>End Journey Reson</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Start Loccation</th>
				<th>End Location</th>
				<th>Total</th>
				<th>General</th>
				<th>Accredited driving School</th>
				<th>Nighttime</th>
				<th>Actions</th>
			</tr>
		</thead>
	
		<tbody>
	<?php
	//  							print_r($results); die;
									if (count ( $results ) > 0) {
										$query_learnerinfo = new ParseQuery ( 'learnersInfo' );
										$query_SupervisorInformation = new ParseQuery ( 'SupervisorInformation' );
										$query_vehicleInformation = new ParseQuery ( 'vehicleInformation' );
	//									for($i = 0; $i < count ( $results ); $i ++) {
										for($i = 0; $i < 1; $i ++) {
											$object = $results [$i];
	?>
				<tr>
				<td>
	<?php
											echo $object->get ( 'learner' );
	?>
				</td>
				<td>
	<?php
											$query_learnerinfo->equalTo ( "byUser", $object->get ( 'byUser' ) );
											$getLearnerinfo = $query_learnerinfo->find ();
											echo $getLearnerinfo[0]->get('learnersLicenseNumber');
	?>
				</td>
				<td>
	<?php
											echo $object->get ( 'supervisor' );
	?>
				</td>
				<td>
	<?php
											$query_SupervisorInformation->equalTo ( "byUser", $object->get ( 'byUser' ) );
											$supervisorInformation = $query_SupervisorInformation->find ();
											echo $supervisorInformation [0]->get ( 'supervisorsLicenseNumber' );
	?>
				</td>
				<td>
	<?php
											echo $object->get ( 'drivingMode' );
	?>
				</td>
				<td>
	<?php
											echo $object->get ( 'drivingExperience' );
	?>
				</td>
				<td>
	<?php
											echo $object->get ( 'durationOfTrip' );
	?>
				</td>
	
				<td class="center">
	<?php
											echo $object->get ( 'vehicle' );
	?>
				</td>
				<td>
	<?php
											echo $object->get ( 'endJourneyReason' );
	?>
				</td>
				<td>
	<?php
											if ($object->get ( 'initialTime' )) {
												echo $object->get ( 'initialTime' )->format ( 'Y-m-d H:i:s' );
											} else {
												echo 'Not Mentioned';
											}
	?>
				</td>
				<td>
	<?php
											if ($object->get ( 'endingTime' )) {
												echo $object->get ( 'endingTime' )->format ( 'Y-m-d H:i:s' );
											} else {
												echo 'Not Mentioned';
											}
	?>
				</td>
				<td>
	<?php
											echo $object->get ( 'startLocationLatitude' ) . ' ' . $object->get ( 'startLocationLongitude' );
	?>	
				</td>
				<td>
	<?php
											echo $object->get ( 'endLocationLatitude' ) . ' ' . $object->get ( 'endLocationLongitude' );
	?>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td class="center">
					<div style = 'width: 100px;'>
						<a title="Generate PDF Report" class="btn btn-success" target="blank" href="<?php // echo LIVE_SITE;?>pdf/docs/report.php?id=<?php echo $object->getObjectId (); ?>">
						<i class="halflings-icon download-icon white zoom-in"></i> </a>
						<a title="View Details" class="btn btn-success" target="blank" href="<?php // echo LIVE_SITE;?>view_logbook_entry.php?id=<?php echo $object->getObjectId (); ?>">
						<i class="halflings-icon white zoom-in"></i> </a>
					</div>	
				</td>
			</tr>
	<?php
			}
		} 
		else 
		{
	?>
	<tr>
				<td>No Result Found!</td>
			</tr>
	<?php
		}
	?>                                
	</tbody>
	</table>
	</div>
	</div>
	<!--/span--></div>
	<!--/row--></div>
	<!--/.fluid-container-->
	
	<!-- end: Content -->
	</div>
	<!--/#content.span10-->
	</div>
	<!--/fluid-row-->
	
	<div class="modal hide fade" id="myModal">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">×</button>
	<h3>Settings</h3>
	</div>
	<div class="modal-body">
	<p>Here settings can be configured...</p>
	</div>
	<div class="modal-footer"><a href="#" class="btn" data-dismiss="modal">Close</a>
	<a href="#" class="btn btn-primary">Save changes</a></div>
	</div>
	
<?php
}
	include ('footer.php');
?>