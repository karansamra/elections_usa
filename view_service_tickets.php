<?php
include('header.php');
//$sql="SELECT * FROM service_tickets";
/*$sql = "SELECT service_tickets.*, poll_venues.ward, poll_venues.address_line_1, poll_venues.address_line_2, poll_venues.ST, poll_venues.ZIP 
FROM service_tickets
LEFT JOIN poll_venues
ON service_tickets.polling_site_id=poll_venues.id
ORDER BY service_tickets.id"; 
*/
 $sql = "SELECT ST.*, PV.ward, PV.address_line_1, PV.voting_district, PV.ST, PV.ZIP ,TH.first_name, TH.last_name, TH.email
FROM service_tickets  ST

LEFT JOIN poll_venues PV
ON ST.polling_site_id=PV.id

LEFT JOIN technician TH
ON ST.technician_id=TH.id

ORDER BY ST.created_at DESC"; //die;

$result=mysqli_query($db,$sql);


?>
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Service Tickets</a></li>
			</ul>
			<a href="assign_polling_venues.php">Add Service Ticket</a>
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Service Tickets</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th width="8%">Technician Name</th>
								  <th width="7%">Email</th>
								  <th width="20%">Polling Site</th>
								  <th width="10%">Voting District</th>
								  <th width="5%">Priority [1-Lowest, 5-Highest]</th>
								  <th width="15%">Reason Of Call</th>
								  <th width="10%">Status</th>
								  <th width="10%">Date Created</th>
								  <th width="15%">Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
						  <?php
  while($results_users=mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
   $object = $results_users;
	$id = $object['id'];
	$address_poll_site = $object['address_line_1'].', '.$object['ST'].', '.$object['ZIP'];
	if($object['status'] == '0'){
		$status = "<span style='color: green;'>Open</span>";
		}
		else{
		$status = "<span style='color: red;'>Closed</span>";
		
		}
  ?>
							<tr id="tr_<?php echo $id; ?>">
								<td><?php echo $object['first_name'].' '.$object['last_name'];?></td>
								 <td><?php echo $object['email']; ?></td>
								<td><?php echo $address_poll_site; ?></td>
								<td><?php echo  $object['voting_district']; ?></td>
								<td><?php echo  $object['priority_ticket']; ?></td>
								<td class="center">
									<?php echo $object['reason_call']; ?>
								</td>
								<td class="center">
									<?php echo $status; ?>
								</td>
								<td class="center">
									<?php echo $object['created_at']; ?>
								</td>
								<td class="center">
									
									
									<a class="btn btn-danger" title="Delete" href="javascript:void(0);" onclick="del_officer('<?php echo $id; ?>')">
										<i class="halflings-icon white trash"></i> 
									</a>
									<a  target = "blank" title="View PDF" href="<?php echo LIVE_SITE; ?>/pdf/docs/service_tkt_pdf.php?id=<?php echo $id; ?>">
										<img src="<?php echo LIVE_SITE; ?>/img/pdf.png"  />
									</a>
								</td>
							</tr>
							     <?php
}?>                                
							  </tbody>
						 </table>  
						  
					</div>
				</div><!--/span-->
			</div><!--/row-->
    

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	<script>
	function del_officer(id){
	var url  = "<?php echo LIVE_SITE; ?>/process/delete_service_ticket.php";

	var confirmm = confirm("Are you sure to delete?");
	//var r = confirm("Press a button!");
	if(confirmm == true){
	
	$.ajax({
	url: url,
	method: "POST",
	data: { del_id : id },
	dataType: "html",
	success: function(data){
	if(data =='success'){
	
		$("tr#tr_"+id).remove();
	}
	}
	});
	}
}
	</script>
<?php
include('footer.php');
?>